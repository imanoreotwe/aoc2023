section .rodata
    filename db "test.txt", 0
    red_length db 0x03
    blue_length db 0x04
    green_length db 0x05

section .text
    global _start

_start:

    ;;  open the file
    mov rax, 2                  ; call OPEN
    mov rdi, filename           ; pathname
    mov rsi, 0                  ; flags
    syscall

    ;; make buffer on the stack
    sub rsp, 64                 ; allocate 64 bytes

    mov rdi, rax                ; store fp
    mov rsi, rsp                ; buff loc
    mov rdx, 64                 ; buff len
    call main

;;; main
;;; loop to run through
;;;
;;; inputs:
;;;     rdi : fp
;;;     rsi : buff location
;;;     rdx : buff len
;;; outputs:
;;;     rax : total calculation
main:
    push rbp
    mov rbp, rsp
    ;; setup call to read_file
    push rdi                    ; fp
    push rsi                    ; buff loc
    push rdx                    ; buff total len
    push rsi                    ; curr buff ptr
    push 0                      ; total
    push 0                      ; buff fill size
    push 0                      ; curr line len

    ;; args should already be set
    mov rcx, -1
    call read_file

    cmp rax, 0
    je main_end

    mov [rsp+8], rax

main_loop:
;;; STACK STATE (location is 8*index)
;;; 6: fp
;;; 5: buff loc
;;; 4: buff total len
;;; 3: curr buff ptr
;;; 2: total
;;; 1: curr buff size
;;; 0: curr line len <- rsp


    ;; call find_newline
    mov rdi, [rsp+48]           ; fp (arg 1)
    mov rsi, [rsp+40]           ; buff ptr (arg 2)
    mov rdx, [rsp+32]           ; buff len (arg 3)
    mov rcx, [rsp+24]           ; curr buff ptr (arg 4)
    mov r8, rsp
    add r8, 8                   ; curr buff size ptr (arg 5)
    call find_newline

    cmp rax, 0
    jle main_end_err

    mov [rsp], rax              ; save curr line len

    ;; call calcluate line
    mov rdi, rsp
    add rdi, 24                 ; curr buff ptr ptr (arg 1)
    mov rsi, rsp
    add rsi, 8                  ; curr buff size ptr (arg 2)
    call calculate_line

    ;; check result
    ;; if > 0 add to total
    ;; increment buffer and keep looping

main_end_err:
    ;; uhoh stinky
main_end:


;;; calculate_line
;;; reads buffer and returns line number to indicate if the line is valid or not
;;; NOTE: rules hardcoded for simplicity
;;;
;;; inputs:
;;;     rdi : buff ptr ptr
;;;     rsi : buff len ptr
;;; output:
;;;     rax : line number for true, 0 for false, < 0 for errors
calculate_line:
    push rbp
    mov rbp, rsp

    ;; store regs
    push rdi
    push rsi
    call find_line_number        ; regs are the same

    pop rsi
    pop rdi

calculate_line_loop:
    push rdi
    push rsi

    mov dil, [rdi]
    cmp dil, 0x3b
    jne calculate_line_loop_num

    mov

calculate_line_loop_num:
    
    ;; grab next number
    xor rdi, rdi
    mov dil, [rsp+8]
    call compare_num

    cmp rax, -1
    je calculate_line_err
    push rax

    ;; grab next color (green)
    mov dil, [rsp]
    cmp dil, 0x67
    jne calculate_line_loop_red

    mov rcx, [rsp]
    sub rcx, 5
    mov [rsp], rcx

    mov rcx, [rsp+8]
    add rcx, 5
    mov [rsp+8], rcx

    cmp rax, 13
    jle calculate_line_loop

    mov rax, -1
    jmp calculate_line_end

calculate_line_loop_red:
    cmp dil, 0x52
    jne calculate_line_loop_blue

    mov rcx, [rsp]
    sub rcx, 3
    mov [rsp], rcx

    mov rcx, [rsp+8]
    add rcx, 5
    mov [rsp+8], rcx

    cmp rax, 12
    jle calculate_line_loop

    mov rax, -1
    jmp calculate_line_end

calculate_line_loop_blue:
    cmp dil, 0x62
    jne calculate_line_err

    mov rcx, [rsp]
    sub rcx, 4
    mov [rsp], rcx

    mov rcx, [rsp+8]
    add rcx, 4
    mov [rsp+8], rcx

    cmp rax, 14
    jle calculate_line_loop

    mov rax, -1
    jmp calculate_line_end
    ;; loooop

calculate_line_err:
    ;; uh oh stinky
calculate_line_end:

    mov rsp, rbp
    pop rbp
    ret


;;; find_line_number
;;; finds the line number and returns it
;;;
;;; inputs:
;;;     rdi : buff ptr ptr (starting at "Game xxx ...")
;;;     rsi : buff len ptr
;;; output:
;;;     rax : line number
find_line_number:
    push rbp
    mov rbp, rsp

    push rdi                    ; save buff ptr location
    push rsi                    ; save len ptr location

    mov rdi, [rdi]              ; move buff ptr into rdi
    mov rsi, [rsi]              ; move buff len into rsi

    ;; move buffer to first number char
    add rdi, 5
    sub rsi, 5

    xor rcx, rcx                ; clear rcx

find_line_number_loop:
    push rdi
    push rsi
    push rcx

    ;; call compare_num on next numbero
    mov r8, rdi
    xor rdi, rdi
    mov dil, [r8]
    call compare_num

    pop rcx
    pop rsi
    pop rdi

    cmp rax, -1
    je find_line_number_end

    ;; increment buff and decement length
    add rdi, 1
    sub rsi, 1

    cmp rcx, 0
    jg find_line_number_digit2

    mov rcx, rax
    jmp find_line_number_loop
find_line_number_digit2:
    imul rcx, 10
    add rcx, rax
    jmp find_line_number_loop
find_line_number_end:
    mov rax, rcx

    ;; increment 2 more times (should be pointing to first number)
    add rdi, 2
    sub rsi, 2

    ;; store updated buffer ptr and length
    mov [rdi], rdi
    pop rdi
    mov [rdi], rsi

    add rsp, 1

    mov rsp, rbp
    pop rbp
    ret

;;; find_newline
;;; read buffer and find the next newline charecter.
;;; if no newline is found, the buffer will be refreshed
;;; inputs:
;;;     rdi : fp
;;;     rsi : buff ptr
;;;     rdx : buff len
;;;     rcx : curr buff ptr
;;;     r8 : curr buff size ptr
;;; outputs:
;;;     rax : length to newline, or -1 for eof
find_newline:
    ;; setup
    push rbp
    mov rbp, rsp

    ;; save values
    push rdi
    push rsi
    push rdx
    push rcx
    push r8

find_newline_inner:
    ;; call find_newline_index
    mov rdi, rsi                ; buff (arg 1)
    mov rsi, [r8]               ; buff size (arg 2)
    call find_newline_index

    cmp rax, -1
    jg find_newline_end

    ;; setup readfile call
    mov rdi, [rsp+32]           ; fp (arg 1)
    mov rsi, [rsp+24]           ; buff ptr (arg 2)
    mov rdx, [rsp+16]           ; buff len (arg 3)

    ;; calc offset (buff loc + size - ptr)
    mov rcx, rsi
    add rcx, [rsp+16]
    sub rcx, [rsp+8]            ; file offset (arg 4)

    call read_file

    ;; error checking lol
    cmp rax, 0
    jle find_newline_err

    mov [r8], rax               ; save new buff fill size
    xor r9, r9
    mov r9, [rsp+24]
    mov [rsp+8], r9             ; reset buff to begining
    jmp find_newline_inner

find_newline_err:
    mov rax, -1
find_newline_end:
    add rsp, 5                  ; reset stack NOTE: check for correctness
    mov rsp, rbp
    pop rbp
    ret

;;; find_newline_index
;;; seeks the buffer to find the next newline char
;;;
;;; inputs:
;;;     rdi : buff
;;;     rsi : buffer len
;;; outputs:
;;;     rax : newline index, -1 if not found
find_newline_index:
    ;; setup
    push rbp
    mov rbp, rsp

    mov rax, 0                  ; iterator
find_newline_loop:
    cmp rax, rsi                ; test loop size
    je find_newline_index_end
    add rax, 1                  ; increment

    mov cl, [rdi+rax-1]        ; store char in %rcx
    cmp cl, 0x0A               ; test char for newline
    jne find_newline_loop       ; jump to loop if \n not found
    sub rax, 1
    ;; cleanup
    mov rsp, rbp
    pop rbp
    ret                         ; else
find_newline_index_end:
    mov rax, -1
    ;; cleanup
    mov rsp, rbp
    pop rbp
    ret

;;; read_file
;;; reads file into buffer
;;; offset is the number of bytes to seek backwards incase the read call is
;;; midline
;;;
;;; inputs:
;;;     rdi : fp
;;;     rsi : buff
;;;     rdx : buff len
;;;     rcx : file offset
;;; output:
;;;     rax : elements read
read_file:
    push rbp
    mov rbp, rsp

    ;; save regs
    push rdi                    ; fp
    push rsi                    ; buff
    push rdx                    ; buff len
    push rcx                    ; file offset

    ;; test seek forward (for eof)
    mov rax, 0                  ; call READ
    mov rdi, rdi                ; set fp (arg 1)
    mov rsi, rsi                ; set buff (arg 2)
    mov rdx, 1                  ; buff len (arg 3)
    syscall

    cmp rax, 0
    jle read_file_end

    ;; check for offset value NOTE: ???????
    mov rsi, [rsp]
    cmp rsi, 0
    jl read_file_adj

    mov rax, 8                  ; call SEEK
    mov rdi, [rsp+24]           ; set fp (arg 1)
    add rsi, 1
    neg rsi                     ; set offset (arg 2)
    mov rdx, 1                  ; SEEK_CUR (arg 3)
    syscall
    ;; NOTE: check for errors
    jmp read_file_rest
read_file_adj:
    ;; adj buff len after seek
    mov rcx, [rsp+8]
    sub rcx, 1
    mov [rsp+8], rcx

    ;; adj buff after seek
    mov rcx, [rsp+16]
    add rcx, 1
    mov [rsp+16], rcx
read_file_rest:
    ;; read into buffer
    mov rax, 0                  ; call READ
    mov rdi, [rsp+24]           ; set fp (arg 1)
    mov rsi, [rsp+16]           ; set buff (arg 2)
    mov rdx, [rsp+8]            ; set buff len (arg 3)
    syscall
    ;; NOTE: check for errors
    cmp rax, 0
    jg read_file_success

    mov rax, -2
    jmp read_file_end
read_file_success:
    add rax, 1
read_file_end:
    mov rsp, rbp
    pop rbp
    ret

;;; compare_num
;;; compares char with number chars
;;;
;;; inputs:
;;;     dil : char
;;; outputs:
;;;     rax : number in integer form
compare_num:
    push rbp
    mov rbp, rsp

    mov rax, -1                 ; iterator
    mov cl, 0x2F
compare_num_loop:
    add rax, 1
    cmp rax, 0xa                ; test loop
    je compare_num_end

    add cl, 1
    cmp cl, dil
    jne compare_num_loop
    mov rsp, rbp
    pop rbp
    ret
compare_num_end:
    mov rax, -1
    mov rsp, rbp
    pop rbp
    ret

