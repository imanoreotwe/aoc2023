section .rodata
    filename db "input.txt", 0

    one_length db 0x03
    one_text db "one", 0

    two_length db 0x03
    two_text db "two", 0

    three_length db 0x05
    three_text db "three", 0

    four_length db 0x04
    four_text db "four", 0

    five_length db 0x04
    five_text db "five", 0

    six_length db 0x03
    six_text db "six", 0

    seven_length db 0x05
    seven_text db "seven", 0

    eight_length db 0x05
    eight_text db "eight", 0

    nine_length db 0x04
    nine_text db "nine", 0

section .text
    global _start

_start:

    ;;  open the file
    mov rax, 2                  ; call OPEN
    mov rdi, filename           ; pathname
    mov rsi, 0                  ; flags
    syscall

    ;; make buffer on the stack
    sub rsp, 64                 ; allocate 64 bytes

    mov rdi, rax                ; store fp
    mov rsi, rsp                ; buff loc
    mov rdx, 64                 ; buff len
    call main_loop

;;; main_loop
;;; loop to run through
;;; NOTE: should make this circular
;;;
;;; inputs:
;;;     rdi : fp
;;;     rsi : buff location
;;;     rdx : buff len
;;; outputs:
;;;     rax : total calculation
main_loop:
    push rbp
    mov rbp, rsp
    ;; setup call to read_file
    push rdi                    ; fp
    push rsi                    ; buff loc
    push rdx                    ; buff total len
    push rsi                    ; curr buff ptr
    push 0                      ; total
    push 0                      ; buff fill size
    push 0                      ; curr line len

    ;; args should already be set
    mov rcx, -1
    call read_file

    cmp rax, 0
    je main_end

    mov [rsp+8], rax

main_continue_0:
;;; STACK STATE (location is 8*index)
;;; 6: fp
;;; 5: buff loc
;;; 4: buff total len
;;; 3: curr buff ptr
;;; 2: total
;;; 1: curr buff size
;;; 0: curr line len <- rsp

    ;; NOTE: check for base case

    ;; call find_newline
    mov rdi, [rsp+24]            ; buff loc (arg 1)
    mov rsi, [rsp+8]            ; curr buff fill size (arg 2)
    call find_newline

    mov [rsp], rax              ; save curr line len

    cmp rax, 0                  ; if \n is the next char we gotta fix
    jne main_post_newline

    mov rcx, [rsp+24]           ; load buff loc
    add rcx, 1                  ; increment it to move past \n
    mov [rsp+24], rcx           ; store that bih

    mov rcx, [rsp+8]            ; load buff size
    sub rcx, 1                  ; decrement it
    mov [rsp+8], rcx            ; store that bih

    jmp main_continue_0

main_post_newline:
    ;; if find_newline returns -1, go back to readfile; else main_continue_1
    cmp rax, -1
    jg main_continue_1

    ;; setup readfile call
    mov rdi, [rsp+48]            ; fp (arg 1)
    mov rsi, [rsp+40]            ; buff ptr (arg 2)
    mov rdx, 64                  ; buff length (arg 3)

    ;; calc the offset (buff loc + size - ptr)

    mov rcx, [rsp+40]
    add rcx, [rsp+32]
    sub rcx, [rsp+24]           ; file offset (arg 4)

    call read_file

    ;; NOTE: add error checking?
    cmp rax, 0
    jle main_end

    mov [rsp+8], rax                ; save new buff fill size
    mov rdx, [rsp+40]
    mov [rsp+24], rdx               ; reset buff to begining
    jmp main_continue_0             ; jump to recalculate new line

main_continue_1:
    mov rdi, [rsp+24]           ; buff
    mov rsi, [rsp]              ; buff len
    call calculate_line

    add rax, [rsp+16]
    mov [rsp+16], rax            ; acc total

    mov rcx, [rsp]              ; old line len
    mov rdx, [rsp+24]            ; old curr ptr
    add rdx, rcx
    add rdx, 1
    mov [rsp+24], rdx            ; save new curr ptr

    mov rcx, [rsp+8]            ; old buff fill size
    mov rdx, [rsp]              ; curr line len
    add rdx, 1                  ; extra for the /n
    sub rcx, rdx
    mov [rsp+8], rcx            ; store new buff fill size NOTE: check for accuracy

    ;; jump to loop
    jmp main_continue_0
main_end:
    mov rax, [rsp+16]
    ;; result should be in rax
    mov rsp, rbp
    pop rbp
    ret
end:
    ;; write result

    ;; exit 0
    mov rax, 60
    mov rdi, 0
    syscall

;;; read_file
;;; reads file into buffer
;;; NOTE: explain file offset?
;;;
;;; inputs:
;;;     rdi : fp
;;;     rsi : buff
;;;     rdx : buff len
;;;     rcx : file offset
;;; output:
;;;     rax : elements read
read_file:
    push rbp
    mov rbp, rsp

    ;; save regs
    push rdi                    ; fp
    push rsi                    ; buff
    push rdx                    ; buff len
    push rcx                    ; file offset

    ;; test seek forward (for eof)
    mov rax, 0                  ; call READ
    mov rdi, rdi                ; set fp (arg 1)
    mov rsi, rsi                ; set buff (arg 2)
    mov rdx, 1                  ; buff len (arg 3)
    syscall

    cmp rax, 0
    jle read_file_end

    ;; check for offset value NOTE: ???????
    mov rsi, [rsp]
    cmp rsi, 0
    jl read_file_adj

    mov rax, 8                  ; call SEEK
    mov rdi, [rsp+24]           ; set fp (arg 1)
    add rsi, 1
    neg rsi                     ; set offset (arg 2)
    mov rdx, 1                  ; SEEK_CUR (arg 3)
    syscall
    ;; NOTE: check for errors
    jmp read_file_rest
read_file_adj:
    ;; adj buff len after seek
    mov rcx, [rsp+8]
    sub rcx, 1
    mov [rsp+8], rcx

    ;; adj buff after seek
    mov rcx, [rsp+16]
    add rcx, 1
    mov [rsp+16], rcx
read_file_rest:
    ;; read into buffer
    mov rax, 0                  ; call READ
    mov rdi, [rsp+24]           ; set fp (arg 1)
    mov rsi, [rsp+16]           ; set buff (arg 2)
    mov rdx, [rsp+8]            ; set buff len (arg 3)
    syscall
    ;; NOTE: check for errors
    cmp rax, 0
    jg read_file_success

    mov rax, -2
    jmp read_file_end
read_file_success:
    add rax, 1
read_file_end:
    mov rsp, rbp
    pop rbp
    ret


;;; calculate_line
;;; reads buffer charecter by charecter until newline
;;; searching for numbers then returns
;;;
;;; inputs:
;;;     rdi : buff
;;;     rsi : buff len
;;; output:
;;;     rax : line total
calculate_line:
    push rbp
    mov rbp, rsp
    ;; store regs
    push -1                     ; iterator
    push rdi                    ; buff
    push rsi                    ; buff len
    push -1                     ; first num
    push -1                     ; second num
calculate_line_loop:
    mov rcx, [rsp+32]           ; load iterator
    add rcx, 1                  ; increment
    mov [rsp+32], rcx           ; store
    mov rdx, [rsp+16]           ; load buff len

    cmp rcx, rdx                ; test loop size
    je calculate_line_end

    xor rdi, rdi                ; make sure to clear reg
    mov rdx, [rsp+24]           ; load buff addr
    mov dil, [rcx+rdx]          ; load char (arg 1)
    call compare_num

    cmp rax, -1                 ; any number?
    jne calculate_line_set

    ;; check for string number
    mov rcx, [rsp+32]           ; load iterator
    mov rdi, [rsp+24]           ; load buff addr
    add rdi, rcx                ; buff ptr (arg 1)
    mov rsi, [rsp+16]           ; load buff len
    sub rsi, rcx                ; set buff len (arg 2)
    call find_num_str

    cmp rax, -1                  ; any number?
    je calculate_line_loop

calculate_line_set:
    ;; if fist num is empty set it
    mov rcx, [rsp+8]
    cmp rcx, -1
    jne calculate_line_num_2

    mov [rsp+8], rax
    jmp calculate_line_loop
calculate_line_num_2:
    ;; store second num and keep looping
    mov [rsp], rax
    jmp calculate_line_loop
calculate_line_end:
    ;; build final calc and return result
    mov rax, [rsp+8]            ; first num
    imul rax, 10
    mov rcx, [rsp]              ; load 2nd num

    cmp rcx, -1
    je calculate_line_end_s
    ;; double number case
    add rax, rcx                ; NOTE: possable off by one here
    jmp calculate_line_end_end
calculate_line_end_s:
    ;; single num case
    add rax, [rsp+8]
calculate_line_end_end:
    mov rsp, rbp
    pop rbp
    ret

;;; compare_num
;;; compares char with number chars
;;;
;;; inputs:
;;;     dil : char
;;; outputs:
;;;     rax : number in integer form
compare_num:
    push rbp
    mov rbp, rsp

    mov rax, -1                 ; iterator
    mov cl, 0x2F
compare_num_loop:
    add rax, 1
    cmp rax, 0xa                ; test loop
    je compare_num_end

    add cl, 1
    cmp cl, dil
    jne compare_num_loop
    mov rsp, rbp
    pop rbp
    ret
compare_num_end:
    mov rax, -1
    mov rsp, rbp
    pop rbp
    ret

;;; find_num_str
;;; searches string for number begining at ptr
;;;
;;; inputs:
;;;     rdi : buff ptr
;;;     rsi : buff len
;;; outputs:
;;;     rax : integer found / -1 if none found
find_num_str:
    push rbp
    mov rbp, rsp
    push r12
    push r13
    push r14

    xor rcx, rcx                ; %rcx for buff (outer) iterator
    xor r12, r12                ; %r12 for mask counter
    xor r13, r13                ; %r13 for mask
    xor r14, r14                ; %r14 for scratch later
    mov r13, 0x1FF              ; setup mask

find_num_loop:                  ; loops through buffer chars
    ;; check inner loop size
    add rcx, 1                  ; increment
    cmp rcx, rsi                ; NOTE: watch for off by one err
    jg find_num_not

    ;; also check if we have gone past 5 (longest number length)
    cmp rcx, 5
    jg find_num_not

    xor rdx, rdx                ; %rdx for number (inner) iterator
    mov r8, one_length          ; %r8 pointer for first number
    mov r9b, [rdi]              ; %r9b char from buff
    add rdi, 1                  ; increment buff
    mov r12, 1                  ; setup mask counter
find_num_inner_loop:            ; loops through each number
    mov r14, r12                ; save r12
    sal r12, 1                  ; increment
                                ;
    ;; check loop size
    add rdx, 1                  ; increment
    cmp rdx, 9                  ; NOTE: watch for off by one
    jg find_num_loop

    ;; check if num str is long enough (compare iterator to length)
    cmp [r8], rcx               ; NOTE: double check this logic...
    je find_num_inner_loop

    mov r10, r8                 ; save r8 for later
    movzx r11, byte [r8]        ; num byte for later
    add r8, 2
    add r8, r11                 ; increment r8

    ;; check if curr num has already been "disqualified"
    and r14, r13
    cmp r14, 0
    je find_num_inner_loop

    add r10, rcx                ; %r10 point to num char
    ;; compare curr char in buff with char in each number
    cmp r9b, [r10]
    je find_num_true

    ;; if false remove from mask
    sub r13, r14
    jmp find_num_inner_loop
find_num_true:
    ;; if true & char is at its total length, this is our number, return
    cmp r11, rcx
    je find_num_end

    jmp find_num_inner_loop

find_num_not:                   ; did not find num :(
    mov rdx, -1
find_num_end:
    mov rax, rdx
    pop r14
    pop r13
    pop r12
    mov rsp, rbp
    pop rbp
    ret

;;; find_newline
;;; seeks the buffer to find the next newline char
;;;
;;; inputs:
;;;     rdi : buff
;;;     rsi : buffer len
;;; outputs:
;;;     rax : newline index, -1 if not found
find_newline:
    ;; setup
    push rbp
    mov rbp, rsp

    mov rax, 0                  ; iterator
find_newline_loop:
    cmp rax, rsi                ; test loop size
    je find_newline_end
    add rax, 1                  ; increment

    mov cl, [rdi+rax-1]        ; store char in %rcx
    cmp cl, 0x0A               ; test char for newline
    jne find_newline_loop       ; jump to loop if \n not found
    sub rax, 1
    ;; cleanup
    mov rsp, rbp
    pop rbp
    ret                         ; else
find_newline_end:
    mov rax, -1
    ;; cleanup
    mov rsp, rbp
    pop rbp
    ret

;;; print_number
;;; prints large number NOTE: not finished :(
;;;
;;; inputs
;;;     rdi : number
;;; outputs:
;;;     rax : result
print_number:
    push rbp
    mov rbp, rsp

    push rdi                    ; store number
    sub rsp, 6                  ; space for chars
    push rsp                    ; pointer to highest digit
