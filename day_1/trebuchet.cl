(defun read_lines (filename)
  "take lines from a file and build an array with them"
  (let ((path (merge-pathnames filename))
        (values '()))
    (with-open-file (s path)
        (do ((l (read-line s) (read-line s nil 'eof)))
            ((eq l 'eof) "eof.")
        (setq values (append values (list l)))))
    (return-from read_lines values)))

(defun get_first_num (string)
  (loop for c across string
        do (let ((num (digit-char-p c)))
             (if num
                 (return-from get_first_num c)))))

(defun calculate_calibration (value)
  (let ((first_num (get_first_num value))
        (last_num (get_first_num (reverse value))))
    (+ (* (digit-char-p first_num) 10)
       (digit-char-p last_num))))

(format t "~d~%" (let ((sum 0))
                   (loop for line in (read_lines "input.txt")
                         do (setq sum (+ sum (calculate_calibration line)))
                         finally (return sum))))
